<?php

namespace Trainig\CustomProductList\Controller\Index;

use Trainig\CustomProductList\Block\Product\CustomList;

class Index extends \Magento\Framework\App\Action\Action
{
	/** @var PageFactory */
    protected $_resultPageFactory;
    
    /** @var  \Magento\Catalog\Model\ResourceModel\Product\Collection */
    protected $_productCollection;
    
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_productCollection = $collectionFactory->create();
        parent::__construct($context);
    }
    public function execute() {
		$result = $this->_resultPageFactory->create();
		
		
		$this->_productCollection->addAttributeToSelect('*')->addAttributeToFilter('status', '1')
			->addAttributeToFilter('rebate', '297');
		
		
		$list = $result->getLayout()->getBlock('rebate.products.list');
        $list->setProductCollection($this->_productCollection);
        
		return $result;
    }
}
